# My i3-gaps config

Hello there, welcome to my i3-gaps config repo.

You are free to use this for your own i3 stuff if you want. Here is some commands you will need for my config to work

## Apps to install.

Here are some commands you will have to type to get the same environment. These are supposed to be for Arch, but you can just replace the pacman and yays to whatever package manager you use.

```
sudo su

pacman -S feh i3-gaps 

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

cd ~

yay -G polybar

mv ~/.config
mkdir polybar
install -Dm644 /usr/local/share/doc/polybar/config $HOME/.config/polybar/config

cd ~
wget https://ibb.co/n7Wckjw
feh --bg-scale endless-shapes.jpg

```

And I think that's it. if you have any questions, or could help me clean this up / make backing up my dotfiles easier, DM me on my [mastodon](https://toot.cafe/@connor).
